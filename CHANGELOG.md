<a name="unreleased"></a>
## [Unreleased]


<a name="v1.0.14"></a>
## [v1.0.14] - 2019-12-01
### Bug Fixes
- add InitService


<a name="v1.0.13"></a>
## [v1.0.13] - 2019-11-06
### Code Refactoring
- update library


<a name="v1.0.12"></a>
## [v1.0.12] - 2019-10-08
### Features
- add using a template for a path to settings in consul


<a name="v1.0.11"></a>
## [v1.0.11] - 2019-08-29
### Code Refactoring
- update library


<a name="v1.0.10"></a>
## [v1.0.10] - 2019-08-06
### Code Refactoring
- update application-settings library


<a name="v1.0.9"></a>
## [v1.0.9] - 2019-08-05
### Features
- add DeregisterCriticalServiceAfter


<a name="v1.0.8"></a>
## [v1.0.8] - 2019-08-05
### Features
- add ip address for consul


<a name="v1.0.7"></a>
## [v1.0.7] - 2019-08-05
### Bug Fixes
- update references


<a name="v1.0.6"></a>
## [v1.0.6] - 2019-08-05
### Features
- add initiating setting for consul service


<a name="v1.0.5"></a>
## [v1.0.5] - 2019-08-05
### Bug Fixes
- fix issue with initiate consul client in watcher


<a name="v1.0.4"></a>
## [v1.0.4] - 2019-07-26
### Features
- consul health check


<a name="v1.0.3"></a>
## [v1.0.3] - 2019-07-25
### Code Refactoring
- improve log information


<a name="v1.0.2"></a>
## [v1.0.2] - 2019-07-25
### Features
- functionality for blocking exit


<a name="v1.0.1"></a>
## v1.0.1 - 2019-07-25
### Features
- basic functionality


[Unreleased]: https://gitlab.com/shadowy/go/micro-service/compare/v1.0.14...HEAD
[v1.0.14]: https://gitlab.com/shadowy/go/micro-service/compare/v1.0.13...v1.0.14
[v1.0.13]: https://gitlab.com/shadowy/go/micro-service/compare/v1.0.12...v1.0.13
[v1.0.12]: https://gitlab.com/shadowy/go/micro-service/compare/v1.0.11...v1.0.12
[v1.0.11]: https://gitlab.com/shadowy/go/micro-service/compare/v1.0.10...v1.0.11
[v1.0.10]: https://gitlab.com/shadowy/go/micro-service/compare/v1.0.9...v1.0.10
[v1.0.9]: https://gitlab.com/shadowy/go/micro-service/compare/v1.0.8...v1.0.9
[v1.0.8]: https://gitlab.com/shadowy/go/micro-service/compare/v1.0.7...v1.0.8
[v1.0.7]: https://gitlab.com/shadowy/go/micro-service/compare/v1.0.6...v1.0.7
[v1.0.6]: https://gitlab.com/shadowy/go/micro-service/compare/v1.0.5...v1.0.6
[v1.0.5]: https://gitlab.com/shadowy/go/micro-service/compare/v1.0.4...v1.0.5
[v1.0.4]: https://gitlab.com/shadowy/go/micro-service/compare/v1.0.3...v1.0.4
[v1.0.3]: https://gitlab.com/shadowy/go/micro-service/compare/v1.0.2...v1.0.3
[v1.0.2]: https://gitlab.com/shadowy/go/micro-service/compare/v1.0.1...v1.0.2
