module gitlab.com/shadowy/go/micro-service

go 1.12

require (
	github.com/hashicorp/consul/api v1.1.0
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.3.0
	gitlab.com/shadowy/go/application-settings v1.0.12
	gitlab.com/shadowy/go/application-settings-consul v1.0.6
)
