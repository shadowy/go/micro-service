package microservice

import (
	"bytes"
	"errors"
	"flag"
	"github.com/hashicorp/consul/api"
	"github.com/sirupsen/logrus"
	consulWatcher "gitlab.com/shadowy/go/application-settings-consul"
	"gitlab.com/shadowy/go/application-settings/settings"
	"gitlab.com/shadowy/go/application-settings/watcher"
	"os"
	"strings"
	"text/template"
)

var settingsMode = flag.String("settings-mode", "file", "mode of settings (possible value: file, consul)")
var settingsSource = flag.String("settings-source", "", "source of settings (path to file or url to consul server)")
var consulPath = flag.String("consul-path", "/settings/{{.Env}}/{{.App}}", "path to setting in consul")
var consulToken = flag.String("consul-token", "", "token for access to consul")

// ConsulClient -  client for consul (if it is using)
var ConsulClient *api.Client

var consulService ConsulService

// ServiceSettings - setup micro-service configuration
func ServiceSettings(creator settings.WatcherSettingsCreator, service ConsulService) (result *settings.Configuration, err error) {
	logrus.WithFields(logrus.Fields{"mode": *settingsMode, "source": *settingsSource}).Info("Service settings")
	consulService = service
	var res settings.Configuration
	var watch settings.Watcher
	switch *settingsMode {
	case "file":
		watch = watcher.NewFileWatcher(*settingsSource)
	case "consul":
		watch, err = getConsulWatcher()
	default:
		err = errors.New("settings-source not properly defined")
	}
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"mode": settingsMode, "source": settingsSource}).Error("Service settings")
	}
	if watch == nil {
		err = errors.New("watcher not defined defined")
		return
	}
	err = watch.Setup(&res, creator)
	if *settingsMode == "consul" {
		consulService.Init(res)
		consulRegistration()
	}
	return &res, err
}

func getConsulWatcher() (watch settings.Watcher, err error) {
	consulConfig := api.DefaultConfig()
	if settingsSource != nil && *settingsSource != "" {
		consulConfig.Address = *settingsSource
	}
	if consulToken != nil && *consulToken != "" {
		consulConfig.Token = *consulToken
	}
	ConsulClient, err = api.NewClient(consulConfig)
	if err != nil {
		return
	}
	path, err := getPath()
	if err != nil {
		return
	}
	return consulWatcher.NewConsulWatcher(ConsulClient, path), nil
}

func getPath() (string, error) {
	var env Env
	env.Env = strings.ToLower(os.Getenv("APP_ENV"))
	if env.Env == "" {
		env.Env = "dev"
	}
	env.App = strings.ToLower(os.Getenv("APP_NAME"))
	if env.App == "" {
		if serviceID != nil {
			env.App = *serviceID
		}
	}
	tmpl, err := template.New("consul-path").Parse(*consulPath)
	if err != nil {
		logrus.WithError(err).Error("getConsulWatcher template")
		return "", err
	}
	var tpl bytes.Buffer
	if err := tmpl.Execute(&tpl, env); err != nil {
		logrus.WithError(err).Error("getConsulWatcher template.execute")
		return "", err
	}
	return tpl.String(), nil
}
