package microservice

import (
	"flag"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/application-settings"
	appSettings "gitlab.com/shadowy/go/application-settings/settings"
	"time"
)

type InitFunction func(config *appSettings.Configuration) error

func InitService(appName string, settingsInst interface{}, initFunction InitFunction, service ConsulService, version string) {
	logrus.Info(fmt.Sprintf("%s v%s - starting", appName, version))
	flag.Parse()
	cfg, err := ServiceSettings(func(data string) (result interface{}, err error) {
		return settings.ApplicationSettings.FromString(settingsInst, data)
	}, service)
	if err != nil {
		logrus.WithError(err).Error(appName + " settings")
		time.Sleep(time.Second * 10)
		logrus.WithError(err).Error(appName + " exit")
		return
	}
	err = initFunction(cfg)
	if err != nil {
		logrus.WithError(err).Error(appName + " initFunction")
		time.Sleep(time.Second * 10)
		logrus.WithError(err).Error(appName + " exit")
		return
	}

	BlockExit(func() {
		logrus.Info(appName + " closed")
	})
}
