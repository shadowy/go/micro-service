package microservice

import (
	"flag"
	"github.com/hashicorp/consul/api"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/application-settings/settings"
	"strconv"
	"time"
)

var serviceEnv = flag.String("service-env", "", "code for environment")
var serviceID = flag.String("service-id", "service-id", "service id")
var serviceName = flag.String("service-name", "service", "name of service")
var serviceIP = flag.String("service-ip", "", "ip address of service")

// ServiceInfo  - service information
type ServiceInfo struct {
	Ports *[]int
}

// ServiceHealthCheck - service health checker
type ServiceHealthCheck struct {
	Name  string
	Notes string
	TTL   time.Duration
	Check func() error
}

// ConsulService - consul service
type ConsulService interface {
	Init(config settings.Configuration)
	GetServiceInfo() ServiceInfo
	GetServiceHealthCheck() []ServiceHealthCheck
}

type consulServiceIDs struct {
	ID   string
	Name string
	Port int
}

func consulRegistration() {
	if consulService == nil {
		return
	}
	ids := getConsulServiceIDs()
	checks := consulService.GetServiceHealthCheck()
	if len(checks) == 0 {
		checks = []ServiceHealthCheck{
			{
				Name:  "Simple check",
				Notes: "Simple checks for service",
				TTL:   time.Second * 20,
				Check: func() error {
					return nil
				},
			},
		}
	}
	for i := range ids {
		id := ids[i]
		registrationInfo := api.AgentServiceRegistration{ID: id.ID, Name: id.Name}
		if id.Port != 0 {
			registrationInfo.Port = id.Port
		}
		if serviceIP != nil && *serviceIP != "" {
			registrationInfo.Address = *serviceIP
		}
		_ = ConsulClient.Agent().ServiceRegister(&registrationInfo)

		for p := range checks {
			check := checks[p]
			serviceCheck := api.AgentCheckRegistration{
				ID:        "check-" + id.ID + "-" + strconv.Itoa(p),
				Name:      check.Name,
				ServiceID: id.ID,
				Notes:     check.Notes,
				AgentServiceCheck: api.AgentServiceCheck{
					TTL:                            check.TTL.String(),
					DeregisterCriticalServiceAfter: (5 * time.Minute).String(),
				},
			}
			_ = ConsulClient.Agent().CheckRegister(&serviceCheck)

			go func() {
				ticker := time.NewTicker(check.TTL / 2)
				for range ticker.C {
					errCheck := check.Check()
					if errCheck != nil {
						logrus.WithError(errCheck).WithFields(logrus.Fields{"ID": serviceCheck.ID, "Name": serviceCheck.Name}).
							Error("ConsulService.check")
						_ = ConsulClient.Agent().FailTTL(serviceCheck.ID, errCheck.Error())
					} else {
						_ = ConsulClient.Agent().PassTTL(serviceCheck.ID, "OK")
					}
				}
			}()
		}
	}
}

func consulRemoveRegistration() {
	if consulService == nil {
		return
	}
	ids := getConsulServiceIDs()
	for i := range ids {
		id := ids[i]
		_ = ConsulClient.Agent().ServiceDeregister(id.ID)
	}
}

func getConsulServiceIDs() []consulServiceIDs {
	if consulService == nil {
		return []consulServiceIDs{}
	}
	info := consulService.GetServiceInfo()
	ports := []int{0}
	if info.Ports != nil && len(*info.Ports) > 0 {
		ports = *info.Ports
	}
	result := make([]consulServiceIDs, len(ports))
	for i := range ports {
		port := ports[i]
		var id, name string
		if port != 0 {
			id = getPrefix() + *serviceID + "-" + strconv.Itoa(port)
			name = *serviceName + " port:" + strconv.Itoa(port)
		} else {
			id = getPrefix() + *serviceID
			name = *serviceName
		}
		result[i] = consulServiceIDs{ID: id, Name: name, Port: port}
	}
	return result
}

func getPrefix() string {
	if serviceEnv == nil || *serviceEnv == "" {
		return ""
	}
	return *serviceEnv + "-"
}
