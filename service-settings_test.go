package microservice

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestServiceSettings(t *testing.T) {
	_, err := ServiceSettings(func(data string) (result interface{}, err error) {
		return nil, nil
	}, nil)
	assert.NotEmpty(t, err, "Error should be not null")
}
